.PHONY: ctan manual

ctan: manual
	mkdir pgf-interference
	cp README.ctan pgf-interference/README
	cp doc/pgf-interference-de.pdf pgf-interference/
	cp doc/pgf-interference-de.tex pgf-interference/
	cp doc/pgf-interference-en.pdf pgf-interference/
	cp doc/pgf-interference-en.tex pgf-interference/
	cp pgf-interference.sty pgf-interference/
	zip -r pgf-interference.zip pgf-interference
	rm -r pgf-interference

manual:
	cd doc && lualatex pgf-interference-de.tex
	cd doc && lualatex pgf-interference-en.tex
	cd doc && lualatex pgf-interference-de.tex
	cd doc && lualatex pgf-interference-en.tex
