The pgf-interference package is a LaTeX package for simulating interference patterns.

The package is available on CTAN: https://ctan.org/pkg/pgf-interference

Run `make manual` to get the English and the German manual as PDF files.

Run `make ctan` to create a ZIP archive for CTAN upload.
